//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Nov 14 09:12:08 2019 by ROOT version 5.34/38
// from TTree trktree/trktree
// found on file: /eos/atlas/atlascerngroupdisk/det-ibl/data/PaperRun2/official/TRACK_NT/data/data17_13TeV.00339957.physics_Main.recon.TRACK_NT.r10258_r10979._0001.pool.root
//////////////////////////////////////////////////////////

#ifndef trktree_h
#define trktree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TProfile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class trktree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           runNumber;
   Int_t           eventNumber;
   Int_t           lumiBlock;
   Float_t         averagePU;
   Float_t         eventPU;
   Int_t           mcFlag;
   Float_t         pileupWeight;
   Float_t         mcWeight;
   Float_t         pVtxX;
   Float_t         pVtxY;
   Float_t         pVtxZ;
   Float_t         pVtxXErr;
   Float_t         pVtxYErr;
   Float_t         pVtxZErr;
   Float_t         truthPVtxX;
   Float_t         truthPVtxY;
   Float_t         truthPVtxZ;
   Int_t           trackNPixelHits;
   Int_t           trackNPixelHoles;
   Int_t           trackNPixelLayers;
   Int_t           trackNPixelOutliers;
   Int_t           trackNIBLHits;
   Int_t           trackNSCTHits;
   Int_t           trackNSCTHoles;
   Int_t           trackNTRTHits;
   Int_t           trackNBLHits;
   Int_t           trackNSplitIBLHits;
   Int_t           trackNSplitBLHits;
   Int_t           trackNSharedIBLHits;
   Int_t           trackNSharedBLHits;
   Int_t           trackNL2Hits;
   Int_t           trackNL3Hits;
   Int_t           trackNECHits;
   Int_t           trackNIBLExpectedHits;
   Int_t           trackNBLExpectedHits;
   Int_t           trackNSCTSharedHits;
   Float_t         trackCharge;
   Float_t         trackPt;
   Float_t         trackPhi;
   Float_t         trackEta;
   Float_t         trackTheta;
   Float_t         trackqOverP;
   Float_t         trackD0;
   Float_t         trackZ0;
   Float_t         trackD0Err;
   Float_t         trackZ0Err;
   Float_t         trackqOverPErr;
   Float_t         trackDeltaZSinTheta;
   Int_t           trackClass;
   Int_t           trackPassCut;
   Float_t         trackPixeldEdx;
   Int_t           trackNPixeldEdx;
   Float_t         trueD0;
   Float_t         trueZ0;
   Float_t         truePhi;
   Float_t         trueTheta;
   Float_t         trueqOverP;
   Int_t           truePdgId;
   Int_t           trueBarcode;
   Float_t         truthMatchProb;
   Float_t         genVtxR;
   Float_t         genVtxZ;
   Int_t           parentFlavour;
   Float_t         minTrkdR;
   Int_t           hitSplitIBL;
   Int_t           hitSplitBL;
   Int_t           hitSplitL2;
   Int_t           hitSplitL3;
   std::vector<int>  *hitIsEndCap;
   std::vector<int>  *hitIsHole;
   std::vector<int>     *hitIsOutlier;
   std::vector<int>     *hitIs3D;
   std::vector<int>     *hitIsIBL;
   std::vector<int>     *hitLayer;
   std::vector<float>   *hitCharge;
   std::vector<int>     *hitToT;
   std::vector<int>     *hitLVL1A;
   std::vector<int>     *hitNPixelX;
   std::vector<int>     *hitNPixelY;
   std::vector<int>     *hitEtaModule;
   std::vector<int>     *hitPhiModule;
   std::vector<float>   *hitVBias;
   std::vector<float>   *hitVDep;
   std::vector<float>   *hitTemp;
   std::vector<float>   *hitLorentzShift;
   std::vector<int>     *hitIsSplit;
   std::vector<float>   *hitGlobalX;
   std::vector<float>   *hitGlobalY;
   std::vector<float>   *hitGlobalZ;
   std::vector<float>   *hitLocalX;
   std::vector<float>   *hitLocalY;
   std::vector<float>   *g4LocalX;
   std::vector<float>   *g4LocalY;
   std::vector<int>     *g4Barcode;
   std::vector<int>     *g4PdgId;
   std::vector<float>   *g4EnergyDeposit;
   std::vector<float>   *trkLocalX;
   std::vector<float>   *trkLocalY;
   std::vector<float>   *hitLocalErrorX;
   std::vector<float>   *hitLocalErrorY;
   std::vector<float>   *trkLocalErrorX;
   std::vector<float>   *trkLocalErrorY;
   std::vector<float>   *unbiasedResidualX;
   std::vector<float>   *unbiasedResidualY;
   std::vector<float>   *biasedResidualX;
   std::vector<float>   *biasedResidualY;
   std::vector<float>   *unbiasedPullX;
   std::vector<float>   *unbiasedPullY;
   std::vector<float>   *biasedPullX;
   std::vector<float>   *biasedPullY;
   std::vector<float>   *trk2G4DistX;
   std::vector<float>   *trk2G4DistY;
   std::vector<float>   *trk2DistX;
   std::vector<float>   *trk2DistY;
   std::vector<float>   *trkPhiOnSurface;
   std::vector<float>   *trkThetaOnSurface;
   std::vector<int>     *hitNContributingPtcs;
   std::vector<int>     *hitNContributingPU;
   std::vector<int>     *hitContributingPtcsBarcode;
   std::vector<int>     *pixelIBLIndex;
   std::vector<int>     *pixelIBLRow;
   std::vector<int>     *pixelIBLCol;
   std::vector<float>   *pixelIBLCharge;
   std::vector<int>     *pixelIBLToT;
   std::vector<int>     *pixelBLIndex;
   std::vector<int>     *pixelBLRow;
   std::vector<int>     *pixelBLCol;
   std::vector<float>   *pixelBLCharge;
   std::vector<int>     *pixelBLToT;
   std::vector<int>     *pixelL2Index;
   std::vector<int>     *pixelL2Row;
   std::vector<int>     *pixelL2Col;
   std::vector<float>   *pixelL2Charge;
   std::vector<int>     *pixelL2ToT;
   std::vector<int>     *pixelL3Index;
   std::vector<int>     *pixelL3Row;
   std::vector<int>     *pixelL3Col;
   std::vector<float>   *pixelL3Charge;
   std::vector<int>     *pixelL3ToT;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_averagePU;   //!
   TBranch        *b_eventPU;   //!
   TBranch        *b_mcFlag;   //!
   TBranch        *b_pileupWeight;   //!
   TBranch        *b_mcWeight;   //!
   TBranch        *b_pVtxX;   //!
   TBranch        *b_pVtxY;   //!
   TBranch        *b_pVtxZ;   //!
   TBranch        *b_pVtxXErr;   //!
   TBranch        *b_pVtxYErr;   //!
   TBranch        *b_pVtxZErr;   //!
   TBranch        *b_truthPVtxX;   //!
   TBranch        *b_truthPVtxY;   //!
   TBranch        *b_truthPVtxZ;   //!
   TBranch        *b_trackNPixelHits;   //!
   TBranch        *b_trackNPixelHoles;   //!
   TBranch        *b_trackNPixelLayers;   //!
   TBranch        *b_trackNPixelOutliers;   //!
   TBranch        *b_trackNIBLHits;   //!
   TBranch        *b_trackNSCTHits;   //!
   TBranch        *b_trackNSCTHoles;   //!
   TBranch        *b_trackNTRTHits;   //!
   TBranch        *b_trackNBLHits;   //!
   TBranch        *b_trackNSplitIBLHits;   //!
   TBranch        *b_trackNSplitBLHits;   //!
   TBranch        *b_trackNSharedIBLHits;   //!
   TBranch        *b_trackNSharedBLHits;   //!
   TBranch        *b_trackNL2Hits;   //!
   TBranch        *b_trackNL3Hits;   //!
   TBranch        *b_trackNECHits;   //!
   TBranch        *b_trackNIBLExpectedHits;   //!
   TBranch        *b_trackNBLExpectedHits;   //!
   TBranch        *b_trackNSCTSharedHits;   //!
   TBranch        *b_trackCharge;   //!
   TBranch        *b_trackPt;   //!
   TBranch        *b_trackPhi;   //!
   TBranch        *b_trackEta;   //!
   TBranch        *b_trackTheta;   //!
   TBranch        *b_trackqOverP;   //!
   TBranch        *b_trackD0;   //!
   TBranch        *b_trackZ0;   //!
   TBranch        *b_trackD0Err;   //!
   TBranch        *b_trackZ0Err;   //!
   TBranch        *b_trackqOverPErr;   //!
   TBranch        *b_trackDeltaZSinTheta;   //!
   TBranch        *b_trackClass;   //!
   TBranch        *b_trackPassCut;   //!
   TBranch        *b_trackPixeldEdx;   //!
   TBranch        *b_trackNPixeldEdx;   //!
   TBranch        *b_trueD0;   //!
   TBranch        *b_trueZ0;   //!
   TBranch        *b_truePhi;   //!
   TBranch        *b_trueTheta;   //!
   TBranch        *b_trueqOverP;   //!
   TBranch        *b_truePdgId;   //!
   TBranch        *b_trueBarcode;   //!
   TBranch        *b_truthMatchProb;   //!
   TBranch        *b_genVtxR;   //!
   TBranch        *b_genVtxZ;   //!
   TBranch        *b_parentFlavour;   //!
   TBranch        *b_minTrkdR;   //!
   TBranch        *b_hitSplitIBL;   //!
   TBranch        *b_hitSplitBL;   //!
   TBranch        *b_hitSplitL2;   //!
   TBranch        *b_hitSplitL3;   //!
   TBranch        *b_hitIsEndCap;   //!
   TBranch        *b_hitIsHole;   //!
   TBranch        *b_hitIsOutlier;   //!
   TBranch        *b_hitIs3D;   //!
   TBranch        *b_hitIsIBL;   //!
   TBranch        *b_hitLayer;   //!
   TBranch        *b_hitCharge;   //!
   TBranch        *b_hitToT;   //!
   TBranch        *b_hitLVL1A;   //!
   TBranch        *b_hitNPixelX;   //!
   TBranch        *b_hitNPixelY;   //!
   TBranch        *b_hitEtaModule;   //!
   TBranch        *b_hitPhiModule;   //!
   TBranch        *b_hitVBias;   //!
   TBranch        *b_hitVDep;   //!
   TBranch        *b_hitTemp;   //!
   TBranch        *b_hitLorentzShift;   //!
   TBranch        *b_hitIsSplit;   //!
   TBranch        *b_hitGlobalX;   //!
   TBranch        *b_hitGlobalY;   //!
   TBranch        *b_hitGlobalZ;   //!
   TBranch        *b_hitLocalX;   //!
   TBranch        *b_hitLocalY;   //!
   TBranch        *b_g4LocalX;   //!
   TBranch        *b_g4LocalY;   //!
   TBranch        *b_g4Barcode;   //!
   TBranch        *b_g4PdgId;   //!
   TBranch        *b_g4EnergyDeposit;   //!
   TBranch        *b_trkLocalX;   //!
   TBranch        *b_trkLocalY;   //!
   TBranch        *b_hitLocalErrorX;   //!
   TBranch        *b_hitLocalErrorY;   //!
   TBranch        *b_trkLocalErrorX;   //!
   TBranch        *b_trkLocalErrorY;   //!
   TBranch        *b_unbiasedResidualX;   //!
   TBranch        *b_unbiasedResidualY;   //!
   TBranch        *b_biasedResidualX;   //!
   TBranch        *b_biasedResidualY;   //!
   TBranch        *b_unbiasedPullX;   //!
   TBranch        *b_unbiasedPullY;   //!
   TBranch        *b_biasedPullX;   //!
   TBranch        *b_biasedPullY;   //!
   TBranch        *b_trk2G4DistX;   //!
   TBranch        *b_trk2G4DistY;   //!
   TBranch        *b_trk2DistX;   //!
   TBranch        *b_trk2DistY;   //!
   TBranch        *b_trkPhiOnSurface;   //!
   TBranch        *b_trkThetaOnSurface;   //!
   TBranch        *b_hitNContributingPtcs;   //!
   TBranch        *b_hitNContributingPU;   //!
   TBranch        *b_hitContributingPtcsBarcode;   //!
   TBranch        *b_pixelIBLIndex;   //!
   TBranch        *b_pixelIBLRow;   //!
   TBranch        *b_pixelIBLCol;   //!
   TBranch        *b_pixelIBLCharge;   //!
   TBranch        *b_pixelIBLToT;   //!
   TBranch        *b_pixelBLIndex;   //!
   TBranch        *b_pixelBLRow;   //!
   TBranch        *b_pixelBLCol;   //!
   TBranch        *b_pixelBLCharge;   //!
   TBranch        *b_pixelBLToT;   //!
   TBranch        *b_pixelL2Index;   //!
   TBranch        *b_pixelL2Row;   //!
   TBranch        *b_pixelL2Col;   //!
   TBranch        *b_pixelL2Charge;   //!
   TBranch        *b_pixelL2ToT;   //!
   TBranch        *b_pixelL3Index;   //!
   TBranch        *b_pixelL3Row;   //!
   TBranch        *b_pixelL3Col;   //!
   TBranch        *b_pixelL3Charge;   //!
   TBranch        *b_pixelL3ToT;   //!

   trktree(TTree *tree=0);
   virtual ~trktree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TTree* tree, Int_t nevents);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   void Write(const char* nomefile);
   TProfile* effIBL;
   TH1F *hChargeIBL0, *hChargeBLa0, *hChargeLa1, *hChargeLa2;
   TH1F *hChargeCorrIBL0, *hChargeCorrBLa0, *hChargeCorrLa1, *hChargeCorrLa2;
   TH1F *hClusterSizeXIBL, *hClusterSizeYIBL;
   TH1F *hClusterSizeXBLa0, *hClusterSizeYBLa0;
   TH1F *hResidualsXIBL, *hResidualsXBLa0;       //EB   
   TH1F *hTOTIBL, *hTOTBLa0; //EB
   TProfile *hIncidenceAngleIBL; TProfile *hIncidenceAngleBLa0; //EB

};

#endif

#ifdef trktree_cxx
trktree::trktree(TTree *tree) : fChain(0) 
{
   Init(tree);

   effIBL = new TProfile("effIBL","",40,-2.0,2.0);
   hChargeIBL0 = new TH1F("hChargeIBL0","",50,0.,100.);
   hChargeBLa0 = new TH1F("hChargeBLa0","",50,0.,50.);
   hChargeLa1 = new TH1F("hChargeLa1","",50,0.,50.);
   hChargeLa2 = new TH1F("hChargeLa2","",50,0.,50.);
    
   hChargeCorrIBL0 = new TH1F("hChargeCorrIBL0","",50,0.,100.);
   hChargeCorrBLa0 = new TH1F("hChargeCorrBLa0","",50,0.,50.);
   hChargeCorrLa1 = new TH1F("hChargeCorrLa1","",50,0.,50.);
   hChargeCorrLa2 = new TH1F("hChargeCorrLa2","",50,0.,50.);

   hClusterSizeXIBL = new TH1F("hClusterSizeXIBL","",5,0.5,5.5); 
   hClusterSizeYIBL = new TH1F("hClusterSizeYIBL","",5,0.5,5.5);
   hClusterSizeXBLa0 = new TH1F("hClusterSizeXBLa0","",5,0.5,5.5); 
   hClusterSizeYBLa0 = new TH1F("hClusterSizeYBLa0","",5,0.5,5.5);

   hResidualsXIBL = new TH1F("hResidualsXIBL","",40,-0.4,0.4);  //EB
   hResidualsXBLa0 = new TH1F("hResidualsXBLa0","",40,-0.4,0.4);  //EB

   hTOTIBL = new TH1F("hTOTIBL","",50,0.,100.); //EB
   hTOTBLa0 = new TH1F("hTOTBLa0","",50,0.,100.); //EB

   hIncidenceAngleIBL = new TProfile("hIncidenceAngleIBL","",20,-0.2,0.8);  //EB
   hIncidenceAngleBLa0 = new TProfile("hIncidenceAngleBLa0","",20,-0.2,0.8);  //EB
}

trktree::~trktree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();

   delete effIBL;
   delete hChargeIBL0;
   delete hChargeBLa0;
   delete hChargeLa1;
   delete hChargeLa2;

   delete hChargeCorrIBL0;
   delete hChargeCorrBLa0;
   delete hChargeCorrLa1;
   delete hChargeCorrLa2;
    
   delete hClusterSizeXIBL;
   delete hClusterSizeYIBL;
   delete hClusterSizeXBLa0;
   delete hClusterSizeYBLa0;
   delete hResidualsXIBL;  //EB
   delete hResidualsXBLa0;  //EB
   delete hTOTIBL;  //EB   
   delete hTOTBLa0;  //EB
   delete hIncidenceAngleIBL;  //EB
   delete hIncidenceAngleBLa0;  //EB
}

Int_t trktree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t trktree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void trktree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   hitIsEndCap = 0;
   hitIsHole = 0;
   hitIsOutlier = 0;
   hitIs3D = 0;
   hitIsIBL = 0;
   hitLayer = 0;
   hitCharge = 0;
   hitToT = 0;
   hitLVL1A = 0;
   hitNPixelX = 0;
   hitNPixelY = 0;
   hitEtaModule = 0;
   hitPhiModule = 0;
   hitVBias = 0;
   hitVDep = 0;
   hitTemp = 0;
   hitLorentzShift = 0;
   hitIsSplit = 0;
   hitGlobalX = 0;
   hitGlobalY = 0;
   hitGlobalZ = 0;
   hitLocalX = 0;
   hitLocalY = 0;
   g4LocalX = 0;
   g4LocalY = 0;
   g4Barcode = 0;
   g4PdgId = 0;
   g4EnergyDeposit = 0;
   trkLocalX = 0;
   trkLocalY = 0;
   hitLocalErrorX = 0;
   hitLocalErrorY = 0;
   trkLocalErrorX = 0;
   trkLocalErrorY = 0;
   unbiasedResidualX = 0;
   unbiasedResidualY = 0;
   biasedResidualX = 0;
   biasedResidualY = 0;
   unbiasedPullX = 0;
   unbiasedPullY = 0;
   biasedPullX = 0;
   biasedPullY = 0;
   trk2G4DistX = 0;
   trk2G4DistY = 0;
   trk2DistX = 0;
   trk2DistY = 0;
   trkPhiOnSurface = 0;
   trkThetaOnSurface = 0;
   hitNContributingPtcs = 0;
   hitNContributingPU = 0;
   hitContributingPtcsBarcode = 0;
   pixelIBLIndex = 0;
   pixelIBLRow = 0;
   pixelIBLCol = 0;
   pixelIBLCharge = 0;
   pixelIBLToT = 0;
   pixelBLIndex = 0;
   pixelBLRow = 0;
   pixelBLCol = 0;
   pixelBLCharge = 0;
   pixelBLToT = 0;
   pixelL2Index = 0;
   pixelL2Row = 0;
   pixelL2Col = 0;
   pixelL2Charge = 0;
   pixelL2ToT = 0;
   pixelL3Index = 0;
   pixelL3Row = 0;
   pixelL3Col = 0;
   pixelL3Charge = 0;
   pixelL3ToT = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("averagePU", &averagePU, &b_averagePU);
   fChain->SetBranchAddress("eventPU", &eventPU, &b_eventPU);
   fChain->SetBranchAddress("mcFlag", &mcFlag, &b_mcFlag);
   fChain->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
   fChain->SetBranchAddress("mcWeight", &mcWeight, &b_mcWeight);
   fChain->SetBranchAddress("pVtxX", &pVtxX, &b_pVtxX);
   fChain->SetBranchAddress("pVtxY", &pVtxY, &b_pVtxY);
   fChain->SetBranchAddress("pVtxZ", &pVtxZ, &b_pVtxZ);
   fChain->SetBranchAddress("pVtxXErr", &pVtxXErr, &b_pVtxXErr);
   fChain->SetBranchAddress("pVtxYErr", &pVtxYErr, &b_pVtxYErr);
   fChain->SetBranchAddress("pVtxZErr", &pVtxZErr, &b_pVtxZErr);
   fChain->SetBranchAddress("truthPVtxX", &truthPVtxX, &b_truthPVtxX);
   fChain->SetBranchAddress("truthPVtxY", &truthPVtxY, &b_truthPVtxY);
   fChain->SetBranchAddress("truthPVtxZ", &truthPVtxZ, &b_truthPVtxZ);
   fChain->SetBranchAddress("trackNPixelHits", &trackNPixelHits, &b_trackNPixelHits);
   fChain->SetBranchAddress("trackNPixelHoles", &trackNPixelHoles, &b_trackNPixelHoles);
   fChain->SetBranchAddress("trackNPixelLayers", &trackNPixelLayers, &b_trackNPixelLayers);
   fChain->SetBranchAddress("trackNPixelOutliers", &trackNPixelOutliers, &b_trackNPixelOutliers);
   fChain->SetBranchAddress("trackNIBLHits", &trackNIBLHits, &b_trackNIBLHits);
   fChain->SetBranchAddress("trackNSCTHits", &trackNSCTHits, &b_trackNSCTHits);
   fChain->SetBranchAddress("trackNSCTHoles", &trackNSCTHoles, &b_trackNSCTHoles);
   fChain->SetBranchAddress("trackNTRTHits", &trackNTRTHits, &b_trackNTRTHits);
   fChain->SetBranchAddress("trackNBLHits", &trackNBLHits, &b_trackNBLHits);
   fChain->SetBranchAddress("trackNSplitIBLHits", &trackNSplitIBLHits, &b_trackNSplitIBLHits);
   fChain->SetBranchAddress("trackNSplitBLHits", &trackNSplitBLHits, &b_trackNSplitBLHits);
   fChain->SetBranchAddress("trackNSharedIBLHits", &trackNSharedIBLHits, &b_trackNSharedIBLHits);
   fChain->SetBranchAddress("trackNSharedBLHits", &trackNSharedBLHits, &b_trackNSharedBLHits);
   fChain->SetBranchAddress("trackNL2Hits", &trackNL2Hits, &b_trackNL2Hits);
   fChain->SetBranchAddress("trackNL3Hits", &trackNL3Hits, &b_trackNL3Hits);
   fChain->SetBranchAddress("trackNECHits", &trackNECHits, &b_trackNECHits);
   fChain->SetBranchAddress("trackNIBLExpectedHits", &trackNIBLExpectedHits, &b_trackNIBLExpectedHits);
   fChain->SetBranchAddress("trackNBLExpectedHits", &trackNBLExpectedHits, &b_trackNBLExpectedHits);
   fChain->SetBranchAddress("trackNSCTSharedHits", &trackNSCTSharedHits, &b_trackNSCTSharedHits);
   fChain->SetBranchAddress("trackCharge", &trackCharge, &b_trackCharge);
   fChain->SetBranchAddress("trackPt", &trackPt, &b_trackPt);
   fChain->SetBranchAddress("trackPhi", &trackPhi, &b_trackPhi);
   fChain->SetBranchAddress("trackEta", &trackEta, &b_trackEta);
   fChain->SetBranchAddress("trackTheta", &trackTheta, &b_trackTheta);
   fChain->SetBranchAddress("trackqOverP", &trackqOverP, &b_trackqOverP);
   fChain->SetBranchAddress("trackD0", &trackD0, &b_trackD0);
   fChain->SetBranchAddress("trackZ0", &trackZ0, &b_trackZ0);
   fChain->SetBranchAddress("trackD0Err", &trackD0Err, &b_trackD0Err);
   fChain->SetBranchAddress("trackZ0Err", &trackZ0Err, &b_trackZ0Err);
   fChain->SetBranchAddress("trackqOverPErr", &trackqOverPErr, &b_trackqOverPErr);
   fChain->SetBranchAddress("trackDeltaZSinTheta", &trackDeltaZSinTheta, &b_trackDeltaZSinTheta);
   fChain->SetBranchAddress("trackClass", &trackClass, &b_trackClass);
   fChain->SetBranchAddress("trackPassCut", &trackPassCut, &b_trackPassCut);
   fChain->SetBranchAddress("trackPixeldEdx", &trackPixeldEdx, &b_trackPixeldEdx);
   fChain->SetBranchAddress("trackNPixeldEdx", &trackNPixeldEdx, &b_trackNPixeldEdx);
   fChain->SetBranchAddress("trueD0", &trueD0, &b_trueD0);
   fChain->SetBranchAddress("trueZ0", &trueZ0, &b_trueZ0);
   fChain->SetBranchAddress("truePhi", &truePhi, &b_truePhi);
   fChain->SetBranchAddress("trueTheta", &trueTheta, &b_trueTheta);
   fChain->SetBranchAddress("trueqOverP", &trueqOverP, &b_trueqOverP);
   fChain->SetBranchAddress("truePdgId", &truePdgId, &b_truePdgId);
   fChain->SetBranchAddress("trueBarcode", &trueBarcode, &b_trueBarcode);
   fChain->SetBranchAddress("truthMatchProb", &truthMatchProb, &b_truthMatchProb);
   fChain->SetBranchAddress("genVtxR", &genVtxR, &b_genVtxR);
   fChain->SetBranchAddress("genVtxZ", &genVtxZ, &b_genVtxZ);
   fChain->SetBranchAddress("parentFlavour", &parentFlavour, &b_parentFlavour);
   fChain->SetBranchAddress("minTrkdR", &minTrkdR, &b_minTrkdR);
   fChain->SetBranchAddress("hitSplitIBL", &hitSplitIBL, &b_hitSplitIBL);
   fChain->SetBranchAddress("hitSplitBL", &hitSplitBL, &b_hitSplitBL);
   fChain->SetBranchAddress("hitSplitL2", &hitSplitL2, &b_hitSplitL2);
   fChain->SetBranchAddress("hitSplitL3", &hitSplitL3, &b_hitSplitL3);
   fChain->SetBranchAddress("hitIsEndCap", &hitIsEndCap, &b_hitIsEndCap);
   fChain->SetBranchAddress("hitIsHole", &hitIsHole, &b_hitIsHole);
   fChain->SetBranchAddress("hitIsOutlier", &hitIsOutlier, &b_hitIsOutlier);
   fChain->SetBranchAddress("hitIs3D", &hitIs3D, &b_hitIs3D);
   fChain->SetBranchAddress("hitIsIBL", &hitIsIBL, &b_hitIsIBL);
   fChain->SetBranchAddress("hitLayer", &hitLayer, &b_hitLayer);
   fChain->SetBranchAddress("hitCharge", &hitCharge, &b_hitCharge);
   fChain->SetBranchAddress("hitToT", &hitToT, &b_hitToT);
   fChain->SetBranchAddress("hitLVL1A", &hitLVL1A, &b_hitLVL1A);
   fChain->SetBranchAddress("hitNPixelX", &hitNPixelX, &b_hitNPixelX);
   fChain->SetBranchAddress("hitNPixelY", &hitNPixelY, &b_hitNPixelY);
   fChain->SetBranchAddress("hitEtaModule", &hitEtaModule, &b_hitEtaModule);
   fChain->SetBranchAddress("hitPhiModule", &hitPhiModule, &b_hitPhiModule);
   fChain->SetBranchAddress("hitVBias", &hitVBias, &b_hitVBias);
   fChain->SetBranchAddress("hitVDep", &hitVDep, &b_hitVDep);
   fChain->SetBranchAddress("hitTemp", &hitTemp, &b_hitTemp);
   fChain->SetBranchAddress("hitLorentzShift", &hitLorentzShift, &b_hitLorentzShift);
   fChain->SetBranchAddress("hitIsSplit", &hitIsSplit, &b_hitIsSplit);
   fChain->SetBranchAddress("hitGlobalX", &hitGlobalX, &b_hitGlobalX);
   fChain->SetBranchAddress("hitGlobalY", &hitGlobalY, &b_hitGlobalY);
   fChain->SetBranchAddress("hitGlobalZ", &hitGlobalZ, &b_hitGlobalZ);
   fChain->SetBranchAddress("hitLocalX", &hitLocalX, &b_hitLocalX);
   fChain->SetBranchAddress("hitLocalY", &hitLocalY, &b_hitLocalY);
   fChain->SetBranchAddress("g4LocalX", &g4LocalX, &b_g4LocalX);
   fChain->SetBranchAddress("g4LocalY", &g4LocalY, &b_g4LocalY);
   fChain->SetBranchAddress("g4Barcode", &g4Barcode, &b_g4Barcode);
   fChain->SetBranchAddress("g4PdgId", &g4PdgId, &b_g4PdgId);
   fChain->SetBranchAddress("g4EnergyDeposit", &g4EnergyDeposit, &b_g4EnergyDeposit);
   fChain->SetBranchAddress("trkLocalX", &trkLocalX, &b_trkLocalX);
   fChain->SetBranchAddress("trkLocalY", &trkLocalY, &b_trkLocalY);
   fChain->SetBranchAddress("hitLocalErrorX", &hitLocalErrorX, &b_hitLocalErrorX);
   fChain->SetBranchAddress("hitLocalErrorY", &hitLocalErrorY, &b_hitLocalErrorY);
   fChain->SetBranchAddress("trkLocalErrorX", &trkLocalErrorX, &b_trkLocalErrorX);
   fChain->SetBranchAddress("trkLocalErrorY", &trkLocalErrorY, &b_trkLocalErrorY);
   fChain->SetBranchAddress("unbiasedResidualX", &unbiasedResidualX, &b_unbiasedResidualX);
   fChain->SetBranchAddress("unbiasedResidualY", &unbiasedResidualY, &b_unbiasedResidualY);
   fChain->SetBranchAddress("biasedResidualX", &biasedResidualX, &b_biasedResidualX);
   fChain->SetBranchAddress("biasedResidualY", &biasedResidualY, &b_biasedResidualY);
   fChain->SetBranchAddress("unbiasedPullX", &unbiasedPullX, &b_unbiasedPullX);
   fChain->SetBranchAddress("unbiasedPullY", &unbiasedPullY, &b_unbiasedPullY);
   fChain->SetBranchAddress("biasedPullX", &biasedPullX, &b_biasedPullX);
   fChain->SetBranchAddress("biasedPullY", &biasedPullY, &b_biasedPullY);
   fChain->SetBranchAddress("trk2G4DistX", &trk2G4DistX, &b_trk2G4DistX);
   fChain->SetBranchAddress("trk2G4DistY", &trk2G4DistY, &b_trk2G4DistY);
   fChain->SetBranchAddress("trk2DistX", &trk2DistX, &b_trk2DistX);
   fChain->SetBranchAddress("trk2DistY", &trk2DistY, &b_trk2DistY);
   fChain->SetBranchAddress("trkPhiOnSurface", &trkPhiOnSurface, &b_trkPhiOnSurface);
   fChain->SetBranchAddress("trkThetaOnSurface", &trkThetaOnSurface, &b_trkThetaOnSurface);
   fChain->SetBranchAddress("hitNContributingPtcs", &hitNContributingPtcs, &b_hitNContributingPtcs);
   fChain->SetBranchAddress("hitNContributingPU", &hitNContributingPU, &b_hitNContributingPU);
   fChain->SetBranchAddress("hitContributingPtcsBarcode", &hitContributingPtcsBarcode, &b_hitContributingPtcsBarcode);
   fChain->SetBranchAddress("pixelIBLIndex", &pixelIBLIndex, &b_pixelIBLIndex);
   fChain->SetBranchAddress("pixelIBLRow", &pixelIBLRow, &b_pixelIBLRow);
   fChain->SetBranchAddress("pixelIBLCol", &pixelIBLCol, &b_pixelIBLCol);
   fChain->SetBranchAddress("pixelIBLCharge", &pixelIBLCharge, &b_pixelIBLCharge);
   fChain->SetBranchAddress("pixelIBLToT", &pixelIBLToT, &b_pixelIBLToT);
   fChain->SetBranchAddress("pixelBLIndex", &pixelBLIndex, &b_pixelBLIndex);
   fChain->SetBranchAddress("pixelBLRow", &pixelBLRow, &b_pixelBLRow);
   fChain->SetBranchAddress("pixelBLCol", &pixelBLCol, &b_pixelBLCol);
   fChain->SetBranchAddress("pixelBLCharge", &pixelBLCharge, &b_pixelBLCharge);
   fChain->SetBranchAddress("pixelBLToT", &pixelBLToT, &b_pixelBLToT);
   fChain->SetBranchAddress("pixelL2Index", &pixelL2Index, &b_pixelL2Index);
   fChain->SetBranchAddress("pixelL2Row", &pixelL2Row, &b_pixelL2Row);
   fChain->SetBranchAddress("pixelL2Col", &pixelL2Col, &b_pixelL2Col);
   fChain->SetBranchAddress("pixelL2Charge", &pixelL2Charge, &b_pixelL2Charge);
   fChain->SetBranchAddress("pixelL2ToT", &pixelL2ToT, &b_pixelL2ToT);
   fChain->SetBranchAddress("pixelL3Index", &pixelL3Index, &b_pixelL3Index);
   fChain->SetBranchAddress("pixelL3Row", &pixelL3Row, &b_pixelL3Row);
   fChain->SetBranchAddress("pixelL3Col", &pixelL3Col, &b_pixelL3Col);
   fChain->SetBranchAddress("pixelL3Charge", &pixelL3Charge, &b_pixelL3Charge);
   fChain->SetBranchAddress("pixelL3ToT", &pixelL3ToT, &b_pixelL3ToT);
   Notify();
}

Bool_t trktree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void trktree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t trktree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef trktree_cxx
