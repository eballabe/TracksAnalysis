# TracksAnalysis

Analysis tool to analyse track events coming fron tuples. It is designated as the subsequent step of ZmumuTuple which produces tuples.

# Run
In a ROOT environment, do:

.L trktree.C+
.L run.C
main()

# Plot 

 .L plot.C

and then choose the quanity you want to plot, e.g.

plot_clustersize()


